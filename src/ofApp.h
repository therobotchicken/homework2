#pragma once

#include "ofMain.h"

#define NSPHERESROW 20
#define NSPHERESCOLUMN 5

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	ofSpherePrimitive spheres[NSPHERESROW][NSPHERESCOLUMN];
	ofColor sphereColors[NSPHERESROW][NSPHERESCOLUMN];
	ofEasyCam cam;
	ofLight light;
	ofMaterial mat;

	float time;
};
