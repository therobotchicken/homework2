#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	ofBackground(0);

	for(int x = 0; x < NSPHERESROW; x++)
		for (int y = 0; y < NSPHERESCOLUMN; y++)
		{
			spheres[x][y].setPosition(glm::vec3((x * 40) - 400, (y * 40) - 400, -400));
			spheres[x][y].set(20, 20);
			sphereColors[x][y].setHsb(150, 255, 255);
		}

	//MESS AROUND WITH THIS
	cam.setDistance(1300);
	ofSetSmoothLighting(true);
	light.setDiffuseColor(ofFloatColor(.95, .95, .65));
	light.setSpecularColor(ofFloatColor(1.f, 1.f, 1.f));
	light.setPosition(-300, 200, 1000);
	mat.setShininess(120);
	mat.setSpecularColor(ofColor(255, 255, 255, 255));
}

//--------------------------------------------------------------
void ofApp::update() {
	time = ofGetElapsedTimeMillis();
	if (time >= 2000)
	{
		ofResetElapsedTimeCounter();
		float noise = 40.0 * ofNoise(time);
		for (int y = 0; y < NSPHERESCOLUMN; y++)
		{
			spheres[0][y].setRadius(spheres[0][y].getRadius() + noise);
		}
	}


	for (int x = NSPHERESROW - 1; x >= 0; x--)
		for (int y = NSPHERESCOLUMN - 1; y >= 0; y--)
		{
			if (x != 0)
			{
				spheres[x][y].setRadius(spheres[x - 1][y].getRadius());
			}
			else
				spheres[x][y].setRadius(20);
		}
}

//--------------------------------------------------------------
void ofApp::draw(){
	cam.begin();
	ofEnableDepthTest();
	ofEnableLighting();
	light.enable();
	//mat.begin();

	ofRotateDeg(90, -1, 0, 0);
	for (int x = 0; x < NSPHERESROW; x++)
		for (int y = 0; y < NSPHERESCOLUMN; y++)
		{
			ofSetColor(sphereColors[x][y]);
			spheres[x][y].draw();
		}

	//mat.end();
	ofDisableLighting();
	ofDisableDepthTest();

	cam.end();

	cam.draw();
}